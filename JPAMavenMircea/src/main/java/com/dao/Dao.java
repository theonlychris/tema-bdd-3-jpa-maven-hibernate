package com.dao;

import java.util.List;
import java.util.Optional;

// TODO: Auto-generated Javadoc
/**
 * Generic interface for DAO purposes
 * All CRUD options to be implemented in classes. 
 * @author Solo
 *
 * @param <T> Generic parameter
 */

public interface Dao<T> {
	
	/**
	 * Gets the.
	 *
	 * @param id the id
	 * @return the optional
	 */
	Optional<T> get(int id);
	
	/**
	 * Gets the all.
	 *
	 * @return the all
	 */
	List<T> getAll();
	
	/**
	 * Creates the.
	 *
	 * @param t the t
	 * @return true, if successful
	 */
	boolean create(T t);
	
	/**
	 * Update.
	 *
	 * @param old the old
	 * @param newObj the new obj
	 * @return true, if successful
	 */
	boolean update(T old, T newObj);
	
	/**
	 * Delete.
	 *
	 * @param t the t
	 * @return true, if successful
	 */
	boolean delete(T t);
}
