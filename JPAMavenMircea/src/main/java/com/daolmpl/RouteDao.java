package com.daolmpl;

import java.util.List;
import java.util.Optional;

import javax.persistence.TypedQuery;

import com.dao.Dao;
import com.entity.Route;
import com.helper.DatabaseHelper;


public class RouteDao implements Dao<Route> {

	private DatabaseHelper databaseHelper;

	public RouteDao(DatabaseHelper databaseHelper) {
		this.databaseHelper = databaseHelper;
	}

	@Override
	public Optional<Route> get(int id) {
		return Optional.ofNullable(databaseHelper.getEntityManager().find(Route.class, id));
	}

	@Override
	public List<Route> getAll() {
		TypedQuery<Route> query = databaseHelper.getEntityManager().createQuery("SELECT r from Route r",
				Route.class);
		return query.getResultList();
	}

	@Override
	public boolean create(Route route) {
		return databaseHelper.executeTransaction(entityManager -> entityManager.persist(route));
	}

	@Override
	public boolean update(Route old, Route newObj) {
		old.setRouteShortName(newObj.getRouteShortName());
		old.setRouteLongName(newObj.getRouteLongName());
		old.setTrains(newObj.getTrains());
		return databaseHelper.executeTransaction(entityManager -> entityManager.merge(old));
	}

	@Override
	public boolean delete(Route route) {
		return databaseHelper.executeTransaction(entityManager -> entityManager.remove(route));
	}

}
