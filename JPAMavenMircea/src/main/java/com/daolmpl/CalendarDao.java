package com.daolmpl;

import java.util.List;
import java.util.Optional;

import javax.persistence.TypedQuery;

import com.dao.Dao;
import com.entity.Calendar;
import com.helper.DatabaseHelper;


public class CalendarDao implements Dao<Calendar> {

	private DatabaseHelper databaseHelper;

	public CalendarDao(DatabaseHelper databaseHelper) {
		this.databaseHelper = databaseHelper;
	}

	@Override
	public Optional<Calendar> get(int id) {
		return Optional.ofNullable(databaseHelper.getEntityManager().find(Calendar.class, id));
	}

	@Override
	public List<Calendar> getAll() {
		TypedQuery<Calendar> query = databaseHelper.getEntityManager().createQuery("SELECT c from Calendar c",
				Calendar.class);
		return query.getResultList();
	}

	@Override
	public boolean create(Calendar calendar) {
		return databaseHelper.executeTransaction(entityManager -> entityManager.persist(calendar));
	}

	@Override
	public boolean update(Calendar old, Calendar newObj) {
		old.setMonday(newObj.getMonday());
		old.setTuesday(newObj.getTuesday());
		old.setWednesday(newObj.getWednesday());
		old.setThursday(newObj.getThursday());
		old.setFriday(newObj.getFriday());
		old.setSaturday(newObj.getSaturday());
		old.setSunday(newObj.getSunday());
		return databaseHelper.executeTransaction(entityManager -> entityManager.merge(old));
	}

	@Override
	public boolean delete(Calendar calendar) {
		return databaseHelper.executeTransaction(entityManager -> entityManager.remove(calendar));
	}

}
