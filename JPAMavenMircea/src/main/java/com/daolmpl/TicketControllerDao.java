package com.daolmpl;

import java.util.List;
import java.util.Optional;

import javax.persistence.TypedQuery;

import com.dao.Dao;
import com.entity.TicketController;
import com.helper.DatabaseHelper;


public class TicketControllerDao implements Dao<TicketController> {

	private DatabaseHelper databaseHelper;

	public TicketControllerDao(DatabaseHelper databaseHelper) {
		this.databaseHelper = databaseHelper;
	}

	@Override
	public Optional<TicketController> get(int id) {
		return Optional.ofNullable(databaseHelper.getEntityManager().find(TicketController.class, id));
	}

	@Override
	public List<TicketController> getAll() {
		TypedQuery<TicketController> query = databaseHelper.getEntityManager().createQuery("SELECT t from TicketController t",
				TicketController.class);
		return query.getResultList();
	}

	@Override
	public boolean create(TicketController ticketController) {
		return databaseHelper.executeTransaction(entityManager -> entityManager.persist(ticketController));
	}

	@Override
	public boolean update(TicketController old, TicketController newObj) {
		old.setTicketControllerName(newObj.getTicketControllerName());
		return databaseHelper.executeTransaction(entityManager -> entityManager.merge(old));
	}

	@Override
	public boolean delete(TicketController ticketController) {
		return databaseHelper.executeTransaction(entityManager -> entityManager.remove(ticketController));
	}

}
