package com.daolmpl;

import java.util.List;
import java.util.Optional;

import javax.persistence.TypedQuery;

import com.dao.Dao;
import com.entity.Customer;
import com.helper.DatabaseHelper;


public class CustomerDao implements Dao<Customer> {

	private DatabaseHelper databaseHelper;

	public CustomerDao(DatabaseHelper databaseHelper) {
		this.databaseHelper = databaseHelper;
	}

	@Override
	public Optional<Customer> get(int id) {
		return Optional.ofNullable(databaseHelper.getEntityManager().find(Customer.class, id));
	}

	@Override
	public List<Customer> getAll() {
		TypedQuery<Customer> query = databaseHelper.getEntityManager().createQuery("SELECT c from Customer c",
				Customer.class);
		return query.getResultList();
	}

	@Override
	public boolean create(Customer customer) {
		return databaseHelper.executeTransaction(entityManager -> entityManager.persist(customer));
	}

	@Override
	public boolean update(Customer old, Customer newObj) {
		old.setCustomerName(newObj.getCustomerName());
		old.setTicket(newObj.getTicket());
		return databaseHelper.executeTransaction(entityManager -> entityManager.merge(old));
	}

	@Override
	public boolean delete(Customer customer) {
		return databaseHelper.executeTransaction(entityManager -> entityManager.remove(customer));
	}

}
