package com.daolmpl;

import java.util.List;
import java.util.Optional;

import javax.persistence.TypedQuery;

import com.dao.Dao;
import com.entity.Ticket;
import com.helper.DatabaseHelper;

public class TicketDao implements Dao<Ticket> {

	private DatabaseHelper databaseHelper;

	public TicketDao(DatabaseHelper databaseHelper) {
		this.databaseHelper = databaseHelper;
	}

	@Override
	public Optional<Ticket> get(int id) {
		return Optional.ofNullable(databaseHelper.getEntityManager().find(Ticket.class, id));
	}

	@Override
	public List<Ticket> getAll() {
		TypedQuery<Ticket> query = databaseHelper.getEntityManager().createQuery("SELECT t from Ticket t",
				Ticket.class);
		return query.getResultList();
	}

	@Override
	public boolean create(Ticket ticket) {
		return databaseHelper.executeTransaction(entityManager -> entityManager.persist(ticket));
	}

	@Override
	public boolean update(Ticket old, Ticket newObj) {
		old.setTicketDate(newObj.getTicketDate());
		old.setTicketPrice(newObj.getTicketDate());
		old.setCustomers(newObj.getCustomers());
		return databaseHelper.executeTransaction(entityManager -> entityManager.merge(old));
	}

	@Override
	public boolean delete(Ticket ticket) {
		return databaseHelper.executeTransaction(entityManager -> entityManager.remove(ticket));
	}

}
