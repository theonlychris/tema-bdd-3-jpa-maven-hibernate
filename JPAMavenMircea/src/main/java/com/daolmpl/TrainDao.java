package com.daolmpl;

import java.util.List;
import java.util.Optional;

import javax.persistence.TypedQuery;

import com.dao.Dao;
import com.entity.Train;
import com.helper.DatabaseHelper;


public class TrainDao implements Dao<Train> {
	
	private DatabaseHelper databaseHelper;
	
	public TrainDao(DatabaseHelper databaseHelper) {
		this.databaseHelper = databaseHelper;
	}

	@Override
	public Optional<Train> get(int id) {
		return Optional.ofNullable(databaseHelper.getEntityManager().find(Train.class, id));
	}

	@Override
	public List<Train> getAll() {
		TypedQuery<Train> query = databaseHelper.getEntityManager().createQuery("SELECT t from Train t",
				Train.class);
		return query.getResultList();
	}

	@Override
	public boolean create(Train train) {
		return databaseHelper.executeTransaction(entityManager -> entityManager.persist(train));
	}

	@Override
	public boolean update(Train old, Train newObj) {
		old.setTrainType(newObj.getTrainType());
		old.setAgency(newObj.getAgency());
		old.setCalendar(newObj.getCalendar());
		old.setRoute(newObj.getRoute());
		old.setTimes(newObj.getTimes());
		return databaseHelper.executeTransaction(entityManager -> entityManager.merge(old));
	}

	@Override
	public boolean delete(Train train) {
		return databaseHelper.executeTransaction(entityManager -> entityManager.remove(train));
	}

}
