package com.daolmpl;

import java.util.List;
import java.util.Optional;

import javax.persistence.TypedQuery;

import com.dao.Dao;
import com.entity.Agency;
import com.helper.DatabaseHelper;


public class AgencyDao implements Dao<Agency> {

	private DatabaseHelper databaseHelper;

	public AgencyDao(DatabaseHelper databaseHelper) {
		this.databaseHelper = databaseHelper;
	}

	@Override
	public Optional<Agency> get(int id) {
		return Optional.ofNullable(databaseHelper.getEntityManager().find(Agency.class, id));
	}

	@Override
	public List<Agency> getAll() {
		TypedQuery<Agency> query = databaseHelper.getEntityManager().createQuery("SELECT a from Agency a",
				Agency.class);
		return query.getResultList();
	}

	@Override
	public boolean create(Agency agency) {
		return databaseHelper.executeTransaction(entityManager -> entityManager.persist(agency));
	}

	@Override
	public boolean update(Agency old, Agency newObj) {
		old.setAgencyName(newObj.getAgencyName());

		return databaseHelper.executeTransaction(entityManager -> entityManager.merge(old));
	}

	@Override
	public boolean delete(Agency agency) {
		return databaseHelper.executeTransaction(entityManager -> entityManager.remove(agency));
	}

}
