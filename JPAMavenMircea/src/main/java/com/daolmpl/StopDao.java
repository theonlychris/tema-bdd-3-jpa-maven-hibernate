package com.daolmpl;

import java.util.List;
import java.util.Optional;

import javax.persistence.TypedQuery;

import com.dao.Dao;
import com.entity.Stop;
import com.helper.DatabaseHelper;

public class StopDao implements Dao<Stop> {

	private DatabaseHelper databaseHelper;

	public StopDao(DatabaseHelper databaseHelper) {
		this.databaseHelper = databaseHelper;
	}

	@Override
	public Optional<Stop> get(int id) {
		return Optional.ofNullable(databaseHelper.getEntityManager().find(Stop.class, id));
	}

	@Override
	public List<Stop> getAll() {
		TypedQuery<Stop> query = databaseHelper.getEntityManager().createQuery("SELECT s from Stop s",
				Stop.class);
		return query.getResultList();
	}

	@Override
	public boolean create(Stop stop) {
		return databaseHelper.executeTransaction(entityManager -> entityManager.persist(stop));
	}

	@Override
	public boolean update(Stop old, Stop newObj) {
		old.setStopName(newObj.getStopName());
		old.setTimes(newObj.getTimes());
		return databaseHelper.executeTransaction(entityManager -> entityManager.merge(old));
	}

	@Override
	public boolean delete(Stop stop) {
		return databaseHelper.executeTransaction(entityManager -> entityManager.remove(stop));
	}

}
