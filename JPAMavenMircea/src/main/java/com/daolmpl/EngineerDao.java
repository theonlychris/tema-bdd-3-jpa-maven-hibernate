package com.daolmpl;

import java.util.List;
import java.util.Optional;

import javax.persistence.TypedQuery;

import com.dao.Dao;
import com.entity.Engineer;
import com.helper.DatabaseHelper;


public class EngineerDao implements Dao<Engineer> {

	private DatabaseHelper databaseHelper;

	public EngineerDao(DatabaseHelper databaseHelper) {
		this.databaseHelper = databaseHelper;
	}

	@Override
	public Optional<Engineer> get(int id) {
		return Optional.ofNullable(databaseHelper.getEntityManager().find(Engineer.class, id));
	}

	@Override
	public List<Engineer> getAll() {
		TypedQuery<Engineer> query = databaseHelper.getEntityManager().createQuery("SELECT e from Engineer e",
				Engineer.class);
		return query.getResultList();
	}

	@Override
	public boolean create(Engineer engineer) {
		return databaseHelper.executeTransaction(entityManager -> entityManager.persist(engineer));
	}

	@Override
	public boolean update(Engineer old, Engineer newObj) {
		old.setEngineerName(newObj.getEngineerName());
		old.setEngineerExpertise(newObj.getEngineerExpertise());
		return databaseHelper.executeTransaction(entityManager -> entityManager.merge(old));
	}

	@Override
	public boolean delete(Engineer engineer) {
		return databaseHelper.executeTransaction(entityManager -> entityManager.remove(engineer));
	}

}
