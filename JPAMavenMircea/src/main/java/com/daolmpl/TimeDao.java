package com.daolmpl;

import java.util.List;
import java.util.Optional;

import javax.persistence.TypedQuery;

import com.dao.Dao;
import com.entity.Time;
import com.helper.DatabaseHelper;

public class TimeDao implements Dao<Time> {

	private DatabaseHelper databaseHelper;

	public TimeDao(DatabaseHelper databaseHelper) {
		this.databaseHelper = databaseHelper;
	}

	@Override
	public Optional<Time> get(int id) {
		return Optional.ofNullable(databaseHelper.getEntityManager().find(Time.class, id));
	}

	@Override
	public List<Time> getAll() {
		TypedQuery<Time> query = databaseHelper.getEntityManager().createQuery("SELECT t from Time t",
				Time.class);
		return query.getResultList();
	}

	@Override
	public boolean create(Time time) {
		return databaseHelper.executeTransaction(entityManager -> entityManager.persist(time));
	}

	@Override
	public boolean update(Time old, Time newObj) {
		old.setStop(newObj.getStop());
		old.setStopSequnce(newObj.getStopSequnce());
		old.setArrivalTime(newObj.getArrivalTime());
		old.setDepartureTime(newObj.getDepartureTime());
		old.setTrain(newObj.getTrain());
		return databaseHelper.executeTransaction(entityManager -> entityManager.merge(old));
	}

	@Override
	public boolean delete(Time time) {
		return databaseHelper.executeTransaction(entityManager -> entityManager.remove(time));
	}

}
