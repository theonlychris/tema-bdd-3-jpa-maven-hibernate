package com.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


// TODO: Auto-generated Javadoc
/**
 * The persistent class for the ticket database table.
 * 
 */
@Entity
@NamedQuery(name="Ticket.findAll", query="SELECT t FROM Ticket t")
public class Ticket implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The ticket id. */
	@Id
	@Column(name="ticket_id")
	private int ticketId;

	/** The ticket date. */
	@Column(name="ticket_date")
	private String ticketDate;

	/** The ticket price. */
	@Column(name="ticket_price")
	private String ticketPrice;

	/** The customers. */
	//bi-directional many-to-one association to Customer
	@OneToMany(mappedBy="ticket")
	private List<Customer> customers;

	/** The train. */
	//bi-directional many-to-one association to Train
	@ManyToOne
	private Train train;

	/**
	 * Instantiates a new ticket.
	 */
	public Ticket() {
	}

	/**
	 * Gets the ticket id.
	 *
	 * @return the ticket id
	 */
	public int getTicketId() {
		return this.ticketId;
	}

	/**
	 * Sets the ticket id.
	 *
	 * @param ticketId the new ticket id
	 */
	public void setTicketId(int ticketId) {
		this.ticketId = ticketId;
	}

	/**
	 * Gets the ticket date.
	 *
	 * @return the ticket date
	 */
	public String getTicketDate() {
		return this.ticketDate;
	}

	/**
	 * Sets the ticket date.
	 *
	 * @param ticketDate the new ticket date
	 */
	public void setTicketDate(String ticketDate) {
		this.ticketDate = ticketDate;
	}

	/**
	 * Gets the ticket price.
	 *
	 * @return the ticket price
	 */
	public String getTicketPrice() {
		return this.ticketPrice;
	}

	/**
	 * Sets the ticket price.
	 *
	 * @param ticketPrice the new ticket price
	 */
	public void setTicketPrice(String ticketPrice) {
		this.ticketPrice = ticketPrice;
	}

	/**
	 * Gets the customers.
	 *
	 * @return the customers
	 */
	public List<Customer> getCustomers() {
		return this.customers;
	}

	/**
	 * Sets the customers.
	 *
	 * @param customers the new customers
	 */
	public void setCustomers(List<Customer> customers) {
		this.customers = customers;
	}

	/**
	 * Adds the customer.
	 *
	 * @param customer the customer
	 * @return the customer
	 */
	public Customer addCustomer(Customer customer) {
		getCustomers().add(customer);
		customer.setTicket(this);

		return customer;
	}

	/**
	 * Removes the customer.
	 *
	 * @param customer the customer
	 * @return the customer
	 */
	public Customer removeCustomer(Customer customer) {
		getCustomers().remove(customer);
		customer.setTicket(null);

		return customer;
	}

	/**
	 * Gets the train.
	 *
	 * @return the train
	 */
	public Train getTrain() {
		return this.train;
	}

	/**
	 * Sets the train.
	 *
	 * @param train the new train
	 */
	public void setTrain(Train train) {
		this.train = train;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "Ticket [ticketId=" + ticketId + ", ticketDate=" + ticketDate + ", ticketPrice=" + ticketPrice
				+ ", customers=" + customers + ", train=" + train + "]";
	}

}