package com.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


// TODO: Auto-generated Javadoc
/**
 * The persistent class for the stop database table.
 * 
 */
@Entity
@NamedQuery(name="Stop.findAll", query="SELECT s FROM Stop s")
public class Stop implements Serializable {
	
	/**
	 * Instantiates a new stop.
	 *
	 * @param stopId the stop id
	 * @param stopName the stop name
	 */
	public Stop(int stopId, String stopName) {
		super();
		this.stopId = stopId;
		this.stopName = stopName;
	}

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The stop id. */
	@Id
	@Column(name="stop_id")
	private int stopId;

	/** The stop name. */
	@Column(name="stop_name")
	private String stopName;

	/** The times. */
	//bi-directional many-to-one association to Time
	@OneToMany(mappedBy="stop")
	private List<Time> times;

	/**
	 * Instantiates a new stop.
	 */
	public Stop() {
	}

	/**
	 * Gets the stop id.
	 *
	 * @return the stop id
	 */
	public int getStopId() {
		return this.stopId;
	}

	/**
	 * Sets the stop id.
	 *
	 * @param stopId the new stop id
	 */
	public void setStopId(int stopId) {
		this.stopId = stopId;
	}

	/**
	 * Gets the stop name.
	 *
	 * @return the stop name
	 */
	public String getStopName() {
		return this.stopName;
	}

	/**
	 * Sets the stop name.
	 *
	 * @param stopName the new stop name
	 */
	public void setStopName(String stopName) {
		this.stopName = stopName;
	}

	/**
	 * Gets the times.
	 *
	 * @return the times
	 */
	public List<Time> getTimes() {
		return this.times;
	}

	/**
	 * Sets the times.
	 *
	 * @param times the new times
	 */
	public void setTimes(List<Time> times) {
		this.times = times;
	}

	/**
	 * Adds the time.
	 *
	 * @param time the time
	 * @return the time
	 */
	public Time addTime(Time time) {
		getTimes().add(time);
		time.setStop(this);

		return time;
	}

	/**
	 * Removes the time.
	 *
	 * @param time the time
	 * @return the time
	 */
	public Time removeTime(Time time) {
		getTimes().remove(time);
		time.setStop(null);

		return time;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "Stop [stopId=" + stopId + ", stopName=" + stopName + "]";
	}

}