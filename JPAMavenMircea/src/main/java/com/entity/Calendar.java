package com.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


// TODO: Auto-generated Javadoc
/**
 * The persistent class for the calendar database table.
 * 
 */
@Entity
@NamedQuery(name="Calendar.findAll", query="SELECT c FROM Calendar c")
public class Calendar implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The service id. */
	@Id
	@Column(name="service_id")
	private int serviceId;

	/** The friday. */
	private byte friday;

	/** The monday. */
	private byte monday;

	/** The saturday. */
	private byte saturday;

	/** The sunday. */
	private byte sunday;

	/** The thursday. */
	private byte thursday;

	/** The tuesday. */
	private byte tuesday;

	/** The wednesday. */
	private byte wednesday;

	/** The trains. */
	//bi-directional many-to-one association to Train
	@OneToMany(mappedBy="calendar")
	private List<Train> trains;

	/**
	 * Instantiates a new calendar.
	 */
	public Calendar() {
	}

	/**
	 * Gets the service id.
	 *
	 * @return the service id
	 */
	public int getServiceId() {
		return this.serviceId;
	}

	/**
	 * Sets the service id.
	 *
	 * @param serviceId the new service id
	 */
	public void setServiceId(int serviceId) {
		this.serviceId = serviceId;
	}

	/**
	 * Gets the friday.
	 *
	 * @return the friday
	 */
	public byte getFriday() {
		return this.friday;
	}

	/**
	 * Sets the friday.
	 *
	 * @param friday the new friday
	 */
	public void setFriday(byte friday) {
		this.friday = friday;
	}

	/**
	 * Gets the monday.
	 *
	 * @return the monday
	 */
	public byte getMonday() {
		return this.monday;
	}

	/**
	 * Sets the monday.
	 *
	 * @param monday the new monday
	 */
	public void setMonday(byte monday) {
		this.monday = monday;
	}

	/**
	 * Gets the saturday.
	 *
	 * @return the saturday
	 */
	public byte getSaturday() {
		return this.saturday;
	}

	/**
	 * Sets the saturday.
	 *
	 * @param saturday the new saturday
	 */
	public void setSaturday(byte saturday) {
		this.saturday = saturday;
	}

	/**
	 * Gets the sunday.
	 *
	 * @return the sunday
	 */
	public byte getSunday() {
		return this.sunday;
	}

	/**
	 * Sets the sunday.
	 *
	 * @param sunday the new sunday
	 */
	public void setSunday(byte sunday) {
		this.sunday = sunday;
	}

	/**
	 * Gets the thursday.
	 *
	 * @return the thursday
	 */
	public byte getThursday() {
		return this.thursday;
	}

	/**
	 * Sets the thursday.
	 *
	 * @param thursday the new thursday
	 */
	public void setThursday(byte thursday) {
		this.thursday = thursday;
	}

	/**
	 * Gets the tuesday.
	 *
	 * @return the tuesday
	 */
	public byte getTuesday() {
		return this.tuesday;
	}

	/**
	 * Sets the tuesday.
	 *
	 * @param tuesday the new tuesday
	 */
	public void setTuesday(byte tuesday) {
		this.tuesday = tuesday;
	}

	/**
	 * Gets the wednesday.
	 *
	 * @return the wednesday
	 */
	public byte getWednesday() {
		return this.wednesday;
	}

	/**
	 * Sets the wednesday.
	 *
	 * @param wednesday the new wednesday
	 */
	public void setWednesday(byte wednesday) {
		this.wednesday = wednesday;
	}

	/**
	 * Gets the trains.
	 *
	 * @return the trains
	 */
	public List<Train> getTrains() {
		return this.trains;
	}

	/**
	 * Sets the trains.
	 *
	 * @param trains the new trains
	 */
	public void setTrains(List<Train> trains) {
		this.trains = trains;
	}

	/**
	 * Adds the train.
	 *
	 * @param train the train
	 * @return the train
	 */
	public Train addTrain(Train train) {
		getTrains().add(train);
		train.setCalendar(this);

		return train;
	}

	/**
	 * Removes the train.
	 *
	 * @param train the train
	 * @return the train
	 */
	public Train removeTrain(Train train) {
		getTrains().remove(train);
		train.setCalendar(null);

		return train;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "Calendar [serviceId=" + serviceId + ", friday=" + friday + ", monday=" + monday + ", saturday="
				+ saturday + ", sunday=" + sunday + ", thursday=" + thursday + ", tuesday=" + tuesday + ", wednesday="
				+ wednesday + "]";
	}

}