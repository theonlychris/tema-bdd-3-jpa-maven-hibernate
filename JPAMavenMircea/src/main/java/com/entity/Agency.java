package com.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


// TODO: Auto-generated Javadoc
/**
 * The persistent class for the agency database table.
 * 
 */
@Entity
@NamedQuery(name="Agency.findAll", query="SELECT a FROM Agency a")
public class Agency implements Serializable {
	
	/**
	 * Instantiates a new agency.
	 *
	 * @param agencyId the agency id
	 * @param agencyName the agency name
	 */
	public Agency(int agencyId, String agencyName) {
		super();
		this.agencyId = agencyId;
		this.agencyName = agencyName;
	}

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The agency id. */
	@Id
	@Column(name="agency_id")
	private int agencyId;

	/** The agency name. */
	@Column(name="agency_name")
	private String agencyName;

	/** The trains. */
	//bi-directional many-to-one association to Train
	@OneToMany(mappedBy="agency")
	private List<Train> trains;

	/**
	 * Instantiates a new agency.
	 */
	public Agency() {
	}

	/**
	 * Gets the agency id.
	 *
	 * @return the agency id
	 */
	public int getAgencyId() {
		return this.agencyId;
	}

	/**
	 * Sets the agency id.
	 *
	 * @param agencyId the new agency id
	 */
	public void setAgencyId(int agencyId) {
		this.agencyId = agencyId;
	}

	/**
	 * Gets the agency name.
	 *
	 * @return the agency name
	 */
	public String getAgencyName() {
		return this.agencyName;
	}

	/**
	 * Sets the agency name.
	 *
	 * @param agencyName the new agency name
	 */
	public void setAgencyName(String agencyName) {
		this.agencyName = agencyName;
	}

	/**
	 * Gets the trains.
	 *
	 * @return the trains
	 */
	public List<Train> getTrains() {
		return this.trains;
	}

	/**
	 * Sets the trains.
	 *
	 * @param trains the new trains
	 */
	public void setTrains(List<Train> trains) {
		this.trains = trains;
	}

	/**
	 * Adds the train.
	 *
	 * @param train the train
	 * @return the train
	 */
	public Train addTrain(Train train) {
		getTrains().add(train);
		train.setAgency(this);

		return train;
	}

	/**
	 * Removes the train.
	 *
	 * @param train the train
	 * @return the train
	 */
	public Train removeTrain(Train train) {
		getTrains().remove(train);
		train.setAgency(null);

		return train;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "Agency [agencyId=" + agencyId + ", agencyName=" + agencyName + "]";
	}

}