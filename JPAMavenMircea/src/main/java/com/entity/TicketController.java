package com.entity;

import java.io.Serializable;
import javax.persistence.*;


// TODO: Auto-generated Javadoc
/**
 * The persistent class for the ticket_controller database table.
 * 
 */
@Entity
@Table(name="ticket_controller")
@NamedQuery(name="TicketController.findAll", query="SELECT t FROM TicketController t")
public class TicketController implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The ticket controller id. */
	@Id
	@Column(name="ticket_controller_id")
	private int ticketControllerId;

	/** The ticket controller name. */
	@Column(name="ticket_controller_name")
	private String ticketControllerName;

	/** The train. */
	//bi-directional many-to-one association to Train
	@ManyToOne
	private Train train;

	/**
	 * Instantiates a new ticket controller.
	 */
	public TicketController() {
	}

	/**
	 * Gets the ticket controller id.
	 *
	 * @return the ticket controller id
	 */
	public int getTicketControllerId() {
		return this.ticketControllerId;
	}

	/**
	 * Sets the ticket controller id.
	 *
	 * @param ticketControllerId the new ticket controller id
	 */
	public void setTicketControllerId(int ticketControllerId) {
		this.ticketControllerId = ticketControllerId;
	}

	/**
	 * Gets the ticket controller name.
	 *
	 * @return the ticket controller name
	 */
	public String getTicketControllerName() {
		return this.ticketControllerName;
	}

	/**
	 * Sets the ticket controller name.
	 *
	 * @param ticketControllerName the new ticket controller name
	 */
	public void setTicketControllerName(String ticketControllerName) {
		this.ticketControllerName = ticketControllerName;
	}

	/**
	 * Gets the train.
	 *
	 * @return the train
	 */
	public Train getTrain() {
		return this.train;
	}

	/**
	 * Sets the train.
	 *
	 * @param train the new train
	 */
	public void setTrain(Train train) {
		this.train = train;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "TicketController [ticketControllerId=" + ticketControllerId + ", ticketControllerName="
				+ ticketControllerName + ", train=" + train + "]";
	}

}