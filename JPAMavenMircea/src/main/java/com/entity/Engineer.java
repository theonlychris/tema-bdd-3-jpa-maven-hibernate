package com.entity;

import java.io.Serializable;
import javax.persistence.*;


// TODO: Auto-generated Javadoc
/**
 * The persistent class for the engineer database table.
 * 
 */
@Entity
@NamedQuery(name="Engineer.findAll", query="SELECT e FROM Engineer e")
public class Engineer implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The engineer id. */
	@Id
	@Column(name="engineer_id")
	private int engineerId;

	/** The engineer expertise. */
	@Column(name="engineer_expertise")
	private String engineerExpertise;

	/** The engineer name. */
	@Column(name="engineer_name")
	private String engineerName;

	/** The train. */
	//bi-directional many-to-one association to Train
	@ManyToOne
	private Train train;

	/**
	 * Instantiates a new engineer.
	 */
	public Engineer() {
	}

	/**
	 * Gets the engineer id.
	 *
	 * @return the engineer id
	 */
	public int getEngineerId() {
		return this.engineerId;
	}

	/**
	 * Sets the engineer id.
	 *
	 * @param engineerId the new engineer id
	 */
	public void setEngineerId(int engineerId) {
		this.engineerId = engineerId;
	}

	/**
	 * Gets the engineer expertise.
	 *
	 * @return the engineer expertise
	 */
	public String getEngineerExpertise() {
		return this.engineerExpertise;
	}

	/**
	 * Sets the engineer expertise.
	 *
	 * @param engineerExpertise the new engineer expertise
	 */
	public void setEngineerExpertise(String engineerExpertise) {
		this.engineerExpertise = engineerExpertise;
	}

	/**
	 * Gets the engineer name.
	 *
	 * @return the engineer name
	 */
	public String getEngineerName() {
		return this.engineerName;
	}

	/**
	 * Sets the engineer name.
	 *
	 * @param engineerName the new engineer name
	 */
	public void setEngineerName(String engineerName) {
		this.engineerName = engineerName;
	}

	/**
	 * Gets the train.
	 *
	 * @return the train
	 */
	public Train getTrain() {
		return this.train;
	}

	/**
	 * Sets the train.
	 *
	 * @param train the new train
	 */
	public void setTrain(Train train) {
		this.train = train;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "Engineer [engineerId=" + engineerId + ", engineerExpertise=" + engineerExpertise + ", engineerName="
				+ engineerName + ", train=" + train + "]";
	}

}