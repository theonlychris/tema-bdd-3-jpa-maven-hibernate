package com.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


// TODO: Auto-generated Javadoc
/**
 * The persistent class for the train database table.
 * 
 */
@Entity
@NamedQuery(name="Train.findAll", query="SELECT t FROM Train t")
public class Train implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The train id. */
	@Id
	@Column(name="train_id")
	private int trainId;

	/** The train type. */
	@Column(name="train_type")
	private String trainType;

	/** The engineers. */
	//bi-directional many-to-one association to Engineer
	@OneToMany(mappedBy="train")
	private List<Engineer> engineers;

	/** The tickets. */
	//bi-directional many-to-one association to Ticket
	@OneToMany(mappedBy="train")
	private List<Ticket> tickets;

	/** The ticket controllers. */
	//bi-directional many-to-one association to TicketController
	@OneToMany(mappedBy="train")
	private List<TicketController> ticketControllers;

	/** The times. */
	//bi-directional many-to-one association to Time
	@OneToMany(mappedBy="train")
	private List<Time> times;

	/** The agency. */
	//bi-directional many-to-one association to Agency
	@ManyToOne
	private Agency agency;

	/** The calendar. */
	//bi-directional many-to-one association to Calendar
	@ManyToOne
	private Calendar calendar;

	/** The route. */
	//bi-directional many-to-one association to Route
	@ManyToOne
	private Route route;

	/**
	 * Instantiates a new train.
	 */
	public Train() {
	}

	/**
	 * Gets the train id.
	 *
	 * @return the train id
	 */
	public int getTrainId() {
		return this.trainId;
	}

	/**
	 * Sets the train id.
	 *
	 * @param trainId the new train id
	 */
	public void setTrainId(int trainId) {
		this.trainId = trainId;
	}

	/**
	 * Gets the train type.
	 *
	 * @return the train type
	 */
	public String getTrainType() {
		return this.trainType;
	}

	/**
	 * Sets the train type.
	 *
	 * @param trainType the new train type
	 */
	public void setTrainType(String trainType) {
		this.trainType = trainType;
	}

	/**
	 * Gets the engineers.
	 *
	 * @return the engineers
	 */
	public List<Engineer> getEngineers() {
		return this.engineers;
	}

	/**
	 * Sets the engineers.
	 *
	 * @param engineers the new engineers
	 */
	public void setEngineers(List<Engineer> engineers) {
		this.engineers = engineers;
	}

	/**
	 * Adds the engineer.
	 *
	 * @param engineer the engineer
	 * @return the engineer
	 */
	public Engineer addEngineer(Engineer engineer) {
		getEngineers().add(engineer);
		engineer.setTrain(this);

		return engineer;
	}

	/**
	 * Removes the engineer.
	 *
	 * @param engineer the engineer
	 * @return the engineer
	 */
	public Engineer removeEngineer(Engineer engineer) {
		getEngineers().remove(engineer);
		engineer.setTrain(null);

		return engineer;
	}

	/**
	 * Gets the tickets.
	 *
	 * @return the tickets
	 */
	public List<Ticket> getTickets() {
		return this.tickets;
	}

	/**
	 * Sets the tickets.
	 *
	 * @param tickets the new tickets
	 */
	public void setTickets(List<Ticket> tickets) {
		this.tickets = tickets;
	}

	/**
	 * Adds the ticket.
	 *
	 * @param ticket the ticket
	 * @return the ticket
	 */
	public Ticket addTicket(Ticket ticket) {
		getTickets().add(ticket);
		ticket.setTrain(this);

		return ticket;
	}

	/**
	 * Removes the ticket.
	 *
	 * @param ticket the ticket
	 * @return the ticket
	 */
	public Ticket removeTicket(Ticket ticket) {
		getTickets().remove(ticket);
		ticket.setTrain(null);

		return ticket;
	}

	/**
	 * Gets the ticket controllers.
	 *
	 * @return the ticket controllers
	 */
	public List<TicketController> getTicketControllers() {
		return this.ticketControllers;
	}

	/**
	 * Sets the ticket controllers.
	 *
	 * @param ticketControllers the new ticket controllers
	 */
	public void setTicketControllers(List<TicketController> ticketControllers) {
		this.ticketControllers = ticketControllers;
	}

	/**
	 * Adds the ticket controller.
	 *
	 * @param ticketController the ticket controller
	 * @return the ticket controller
	 */
	public TicketController addTicketController(TicketController ticketController) {
		getTicketControllers().add(ticketController);
		ticketController.setTrain(this);

		return ticketController;
	}

	/**
	 * Removes the ticket controller.
	 *
	 * @param ticketController the ticket controller
	 * @return the ticket controller
	 */
	public TicketController removeTicketController(TicketController ticketController) {
		getTicketControllers().remove(ticketController);
		ticketController.setTrain(null);

		return ticketController;
	}

	/**
	 * Gets the times.
	 *
	 * @return the times
	 */
	public List<Time> getTimes() {
		return this.times;
	}

	/**
	 * Sets the times.
	 *
	 * @param times the new times
	 */
	public void setTimes(List<Time> times) {
		this.times = times;
	}

	/**
	 * Adds the time.
	 *
	 * @param time the time
	 * @return the time
	 */
	public Time addTime(Time time) {
		getTimes().add(time);
		time.setTrain(this);

		return time;
	}

	/**
	 * Removes the time.
	 *
	 * @param time the time
	 * @return the time
	 */
	public Time removeTime(Time time) {
		getTimes().remove(time);
		time.setTrain(null);

		return time;
	}

	/**
	 * Gets the agency.
	 *
	 * @return the agency
	 */
	public Agency getAgency() {
		return this.agency;
	}

	/**
	 * Sets the agency.
	 *
	 * @param agency the new agency
	 */
	public void setAgency(Agency agency) {
		this.agency = agency;
	}

	/**
	 * Gets the calendar.
	 *
	 * @return the calendar
	 */
	public Calendar getCalendar() {
		return this.calendar;
	}

	/**
	 * Sets the calendar.
	 *
	 * @param calendar the new calendar
	 */
	public void setCalendar(Calendar calendar) {
		this.calendar = calendar;
	}

	/**
	 * Gets the route.
	 *
	 * @return the route
	 */
	public Route getRoute() {
		return this.route;
	}

	/**
	 * Sets the route.
	 *
	 * @param route the new route
	 */
	public void setRoute(Route route) {
		this.route = route;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "Train [trainId=" + trainId + ", trainType=" + trainType + ", agency=" + agency + ", calendar="
				+ calendar + ", route=" + route + "]";
	}

}