package com.entity;

import java.io.Serializable;
import javax.persistence.*;


// TODO: Auto-generated Javadoc
/**
 * The persistent class for the time database table.
 * 
 */
@Entity
@NamedQuery(name="Time.findAll", query="SELECT t FROM Time t")
public class Time implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The time id. */
	@Id
	@Column(name="time_id")
	private int timeId;

	/** The arrival time. */
	@Column(name="arrival_time")
	private String arrivalTime;

	/** The departure time. */
	@Column(name="departure_time")
	private String departureTime;

	/** The stop sequnce. */
	@Column(name="stop_sequnce")
	private int stopSequnce;

	/** The stop. */
	//bi-directional many-to-one association to Stop
	@ManyToOne
	private Stop stop;

	/** The train. */
	//bi-directional many-to-one association to Train
	@ManyToOne
	private Train train;

	/**
	 * Instantiates a new time.
	 */
	public Time() {
	}

	/**
	 * Gets the time id.
	 *
	 * @return the time id
	 */
	public int getTimeId() {
		return this.timeId;
	}

	/**
	 * Sets the time id.
	 *
	 * @param timeId the new time id
	 */
	public void setTimeId(int timeId) {
		this.timeId = timeId;
	}

	/**
	 * Gets the arrival time.
	 *
	 * @return the arrival time
	 */
	public String getArrivalTime() {
		return this.arrivalTime;
	}

	/**
	 * Sets the arrival time.
	 *
	 * @param arrivalTime the new arrival time
	 */
	public void setArrivalTime(String arrivalTime) {
		this.arrivalTime = arrivalTime;
	}

	/**
	 * Gets the departure time.
	 *
	 * @return the departure time
	 */
	public String getDepartureTime() {
		return this.departureTime;
	}

	/**
	 * Sets the departure time.
	 *
	 * @param departureTime the new departure time
	 */
	public void setDepartureTime(String departureTime) {
		this.departureTime = departureTime;
	}

	/**
	 * Gets the stop sequnce.
	 *
	 * @return the stop sequnce
	 */
	public int getStopSequnce() {
		return this.stopSequnce;
	}

	/**
	 * Sets the stop sequnce.
	 *
	 * @param stopSequnce the new stop sequnce
	 */
	public void setStopSequnce(int stopSequnce) {
		this.stopSequnce = stopSequnce;
	}

	/**
	 * Gets the stop.
	 *
	 * @return the stop
	 */
	public Stop getStop() {
		return this.stop;
	}

	/**
	 * Sets the stop.
	 *
	 * @param stop the new stop
	 */
	public void setStop(Stop stop) {
		this.stop = stop;
	}

	/**
	 * Gets the train.
	 *
	 * @return the train
	 */
	public Train getTrain() {
		return this.train;
	}

	/**
	 * Sets the train.
	 *
	 * @param train the new train
	 */
	public void setTrain(Train train) {
		this.train = train;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "Time [timeId=" + timeId + ", arrivalTime=" + arrivalTime + ", departureTime=" + departureTime
				+ ", stopSequnce=" + stopSequnce + ", stop=" + stop + ", train=" + train + "]";
	}

}