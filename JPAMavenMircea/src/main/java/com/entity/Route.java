package com.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


// TODO: Auto-generated Javadoc
/**
 * The persistent class for the route database table.
 * 
 */
@Entity
@NamedQuery(name="Route.findAll", query="SELECT r FROM Route r")
public class Route implements Serializable {
	
	/**
	 * Instantiates a new route.
	 *
	 * @param routeId the route id
	 * @param routeLongName the route long name
	 * @param routeShortName the route short name
	 */
	public Route(int routeId, String routeLongName, String routeShortName) {
		super();
		this.routeId = routeId;
		this.routeLongName = routeLongName;
		this.routeShortName = routeShortName;
	}

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The route id. */
	@Id
	@Column(name="route_id")
	private int routeId;

	/** The route long name. */
	@Column(name="route_long_name")
	private String routeLongName;

	/** The route short name. */
	@Column(name="route_short_name")
	private String routeShortName;

	/** The trains. */
	//bi-directional many-to-one association to Train
	@OneToMany(mappedBy="route")
	private List<Train> trains;

	/**
	 * Instantiates a new route.
	 */
	public Route() {
	}

	/**
	 * Gets the route id.
	 *
	 * @return the route id
	 */
	public int getRouteId() {
		return this.routeId;
	}

	/**
	 * Sets the route id.
	 *
	 * @param routeId the new route id
	 */
	public void setRouteId(int routeId) {
		this.routeId = routeId;
	}

	/**
	 * Gets the route long name.
	 *
	 * @return the route long name
	 */
	public String getRouteLongName() {
		return this.routeLongName;
	}

	/**
	 * Sets the route long name.
	 *
	 * @param routeLongName the new route long name
	 */
	public void setRouteLongName(String routeLongName) {
		this.routeLongName = routeLongName;
	}

	/**
	 * Gets the route short name.
	 *
	 * @return the route short name
	 */
	public String getRouteShortName() {
		return this.routeShortName;
	}

	/**
	 * Sets the route short name.
	 *
	 * @param routeShortName the new route short name
	 */
	public void setRouteShortName(String routeShortName) {
		this.routeShortName = routeShortName;
	}

	/**
	 * Gets the trains.
	 *
	 * @return the trains
	 */
	public List<Train> getTrains() {
		return this.trains;
	}

	/**
	 * Sets the trains.
	 *
	 * @param trains the new trains
	 */
	public void setTrains(List<Train> trains) {
		this.trains = trains;
	}

	/**
	 * Adds the train.
	 *
	 * @param train the train
	 * @return the train
	 */
	public Train addTrain(Train train) {
		getTrains().add(train);
		train.setRoute(this);

		return train;
	}

	/**
	 * Removes the train.
	 *
	 * @param train the train
	 * @return the train
	 */
	public Train removeTrain(Train train) {
		getTrains().remove(train);
		train.setRoute(null);

		return train;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "Route [routeId=" + routeId + ", routeLongName=" + routeLongName + ", routeShortName=" + routeShortName
				+ "]";
	}

}