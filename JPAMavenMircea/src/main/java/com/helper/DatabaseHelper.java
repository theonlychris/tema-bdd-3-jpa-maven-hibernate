package com.helper;

import java.util.function.Consumer;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

// TODO: Auto-generated Javadoc
/**
 * The Class DatabaseHelper.
 */
public class DatabaseHelper {

    /**
     * Instantiates a new database helper.
     */
    private DatabaseHelper() {

    }

    /** The instance. */
    private static DatabaseHelper instance;

    /**
     * Gets the single instance of DatabaseHelper.
     *
     * @return single instance of DatabaseHelper
     */
    public static DatabaseHelper getInstance() {
        if (instance == null) {
            instance = new DatabaseHelper();
            instance.init();
        }

        return instance;
    }

    /** The entity manager factory. */
    private EntityManagerFactory entityManagerFactory;
    
    /** The entity manager. */
    private EntityManager entityManager;

    /**
     * Gets the entity manager.
     *
     * @return the entity manager
     */
    public EntityManager getEntityManager() {
        return entityManager;
    }

    /**
     * Execute transaction.
     *
     * @param action the action
     * @return true, if successful
     */
    public boolean executeTransaction(Consumer<EntityManager> action) {
        EntityTransaction entityTransaction = entityManager.getTransaction();
        try {
            entityTransaction.begin();
            action.accept(entityManager);
            entityTransaction.commit();
        } catch (RuntimeException e) {
            System.err.println("Transaction error: " + e.getLocalizedMessage());
            entityTransaction.rollback();
            return false;
        }

        return true;
    }

    /**
     * Inits the.
     *
     * @return true, if successful
     */
    private boolean init() {
        try {
            entityManagerFactory = Persistence.createEntityManagerFactory("JPAMavenMircea");
            entityManager = entityManagerFactory.createEntityManager();
        } catch (Exception e) {
            System.err.println("Error at initialing DatabaseManager: " + e.getMessage().toString());
            return false;
        }

        return true;
    }
}