package com.main;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;

import com.daolmpl.AgencyDao;
import com.daolmpl.RouteDao;
import com.daolmpl.StopDao;
import com.daolmpl.TicketDao;
import com.daolmpl.TrainDao;
import com.entity.Agency;
import com.entity.Route;
import com.entity.Stop;
import com.entity.Ticket;
import com.entity.Train;
import com.helper.DatabaseHelper;

// TODO: Auto-generated Javadoc
/**
 * The Class App.
 */
public class App {
	
	/** The Constant dh. */
	static final DatabaseHelper dh = DatabaseHelper.getInstance();

	/**
	 * Prints the list.
	 *
	 * @param <T> the generic type
	 * @param list the list
	 */
	public static <T> void printList(List<T> list) {
		// Display array elements
		for (T element : list) {
			System.out.printf("%s ", element);
			System.out.println();
		}
		System.out.println();
	}

	/**
	 * Example crud.
	 */
	public static void ExampleCrud() {
		DatabaseHelper dh = DatabaseHelper.getInstance();

		// CRUD - Create - Read - Update - Delete

		/* READ */
		AgencyDao agencyDao = new AgencyDao(dh);
		RouteDao routeDao = new RouteDao(dh);

		List<Agency> agencyList = agencyDao.getAll();

		List<Route> routeList = routeDao.getAll();

		System.out.println("Agency list: ");
		printList(agencyList);

		/* READ */

		/* CREATE */

		System.out.println("Route list before CREATE: ");
		printList(routeList);

		routeList.get(routeList.size() - 1);
		Route aRoute = new Route(routeList.size() + 1, "Covasna:Tulcea", "CV:TL");

		routeDao.create(aRoute);

		routeList = routeDao.getAll();
		System.out.println("Route list after CREATE: ");
		printList(routeList);

		/* CREATE */

		/* UPDATE */

		routeDao.update(aRoute, new Route(routeList.size() + 1, "Cluj:Hunedoara", "CJ:HD"));

		routeList = routeDao.getAll();
		System.out.println("Route list after UPDATE: ");
		printList(routeList);

		/* UPDATE */

		/* DELETE */

		routeDao.delete(aRoute);

		routeList = routeDao.getAll();
		System.out.println("Route list after DELETE: ");
		printList(routeList);

		/* DELETE */
	}

	/**
	 * Menu.
	 */
	private static void Menu() {

		Scanner scanner = new Scanner(System.in);

		while (true) {
			showPageOne();

			int option = scanner.nextInt();

			switch (option) {
			case 1:
				showAgencyMenu();
				break;
			case 2:
				 showRouteMenu();
				break;
			case 3:
				 showStopMenu();
				break;
			default:
				option = -1;
				break;
			}

			if (option == -1)
				break;
		}

		scanner.close();
	}

	/**
	 * Show page one.
	 */
	private static void showPageOne() {

		System.out.println("1. Agency Menu");
		System.out.println("2. Route Menu");
		System.out.println("3. Stop Menu");
		System.out.println("4. Join Example Functions Menu \n");
	}

	/**
	 * Show agency menu.
	 */
	private static void showAgencyMenu() {

		AgencyDao agencyDao = new AgencyDao(dh);
		Scanner scanner = new Scanner(System.in);
		while (true) {
			System.out.println("-- Agency Menu --\n");
			System.out.println("0.Back to Main Menu");
			System.out.println("1.Print list of Agencies");
			System.out.println("2.Create an add a new Agency");
			System.out.println("3.Delete an existing Agency");
			System.out.println("4.Update an existing Agency");

			int option = scanner.nextInt();

			switch (option) {
			case 0:
				Menu();
			case 1:
				printList(agencyDao.getAll());
				break;
			case 2: {
				 System.out.println("Enter the Agency's Id and Name: "); 
				 int id = scanner.nextInt(); 
				 String name = scanner.next(); 
				 agencyDao.create(new Agency(id, name));			 
				break;
			}
			case 3: {
				  System.out.println("Enter the Agency's Id: "); 
				  int id = scanner.nextInt();
				  agencyDao.delete(agencyDao.get(id).get());
				break;
			}
			case 4: {
				 System.out.println("Enter the old Agency's Id"); 
				 int id = scanner.nextInt(); 
				 System.out.println("Enter the new Agency's Name"); 
				 String name = scanner.next();
				 Optional<Agency> old = agencyDao.get(id);
				 Agency oldAgency = new Agency(old.get().getAgencyId(), old.get().getAgencyName());
				 agencyDao.update(oldAgency, new Agency(oldAgency.getAgencyId(), name)); 
				break;
			}
			default:
				break;
			}
		}
	}

	/**
	 * Show route menu.
	 */
	private static void showRouteMenu() {

		RouteDao routeDao = new RouteDao(dh);
		Scanner scanner = new Scanner(System.in);
		while (true)
		{
			System.out.println("-- Route Menu --\n");
			System.out.println("0.Back to Main Menu");
			System.out.println("1.Print list of Routes");
			System.out.println("2.Create an add a new Routes");
			System.out.println("3.Delete an existing Route");
			System.out.println("4.Update an existing Route");

			int option = scanner.nextInt();

			switch (option) {
			case 0:
				Menu();
			case 1:
				printList(routeDao.getAll());
				break;
			case 2:
			{
				System.out.println("Enter the Route's Id and Short Name: ");
				int id = scanner.nextInt();
				String shortName = scanner.next();
				System.out.println("Enter the Route's Long Name: ");
				String longName = scanner.next();
				routeDao.create(new Route(id,shortName,longName));
				break;				
			}
			case 3:
				  System.out.println("Enter the Route's Id: "); 
				  int id = scanner.nextInt();
				  routeDao.delete(routeDao.get(id).get());
				break;
			case 4:
			{				
				 System.out.println("Enter the old Route's Id"); 
				 id = scanner.nextInt(); 
				 System.out.println("Enter the new Route's Short Name"); 		 
				 String shortName = scanner.next();
				 System.out.println("Enter the new Route's Long Name: ");
				 String longName = scanner.next();
				 Optional<Route> old = routeDao.get(id);
				 Route oldRoute = new Route(old.get().getRouteId(), old.get().getRouteLongName(), old.get().getRouteShortName());
				 routeDao.update(oldRoute, new Route(oldRoute.getRouteId(), longName, shortName)); 
				break;	
			}
			default:
				break;
			}
		}
	}
	

	/**
	 * Show stop menu.
	 */
	private static void showStopMenu() {

		StopDao stopDao = new StopDao(dh);
		Scanner scanner = new Scanner(System.in);
		while (true) {
			System.out.println("-- Stop Menu --\n");
			System.out.println("0.Back to Main Menu");
			System.out.println("1.Print list of Stops");
			System.out.println("2.Create an add a new Stop");
			System.out.println("3.Delete an existing Stop");
			System.out.println("4.Update an existing Stop");

			int option = scanner.nextInt();

			switch (option) {
			case 0:
				Menu();
			case 1:
				printList(stopDao.getAll());
				break;
			case 2: {
				 System.out.println("Enter the Stop's Id and Name: "); 
				 int id = scanner.nextInt(); 
				 String name = scanner.next(); 
				 stopDao.create(new Stop(id, name));			 
				break;
			}
			case 3: {
				  System.out.println("Enter the Stop's Id: "); 
				  int id = scanner.nextInt();
				  stopDao.delete(stopDao.get(id).get());
				break;
			}
			case 4: {
				 System.out.println("Enter the old Stop's Id"); 
				 int id = scanner.nextInt(); 
				 System.out.println("Enter the new Stop's Name"); 
				 String name = scanner.next();
				 Optional<Stop> old = stopDao.get(id);
				 Stop oldStop = new Stop(old.get().getStopId(), old.get().getStopName());
				 stopDao.update(oldStop, new Stop(oldStop.getStopId(), name)); 
				break;
			}
			default:
				break;
			}
		}
	}

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 * @throws SQLException the SQL exception
	 * @throws ClassNotFoundException the class not found exception
	 */
	public static void main(String[] args) throws SQLException, ClassNotFoundException {

		// ExampleCrud();

		Menu();

	}
}
